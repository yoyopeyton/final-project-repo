#Peyton Oakes-Bryden
#29 October, 2015
#True Final Project
'''
def unitRequest():
    unitChoice = input('Please enter either meters or feet for your unit of measure...')
    if (meters)
'''

import math
from math import pi

def Runner():

    #MappedObjectDimensions
    print(' ')
    print('Mapped Object: Dimensions')
    print(' ')
    MeasuredObjectLength = int(input('Please enter the Length of the measured object...'))
    MeasuredObjectWidth = int(input('Please enter the Width of the measured object...'))
    MeasuredObjectHeight = float(input('Please enter the Height of the measured object...'))
    print(' ')

    #LayerOneDimensions
    print(' ')
    print('Mapping Layer: 1')
    print(' ')
    LayerOneWidth = int(input('Please enter the Width of first Layer in feet...'))
    LayerOneLength = int(input('Please enter the Length of the first layer in feet...'))
    print(' ')

    #AreaCalculations
    OutputSquareFeet = LayerOneWidth * LayerOneLength
    print(OutputSquareFeet,"Square Feet")
    OutputSquareMiles = OutputSquareFeet / 27878400
    print(OutputSquareMiles,"Square Miles")
    OutputAcres = OutputSquareFeet / 43560
    print(OutputAcres, "Acres")
    print(' ')

    '''
    #TerrainAvoidance nw!!!!!
    print(' ')
    TerrainHeight = int(input('Please enter the MAX Height of the terrain...'))
    if TerrainHeight > MeasuredObjectHeight:
        TerrainHeight == MeasuredObjectHeight

    else:
        MeasuredObjectHeight =  MeasuredObjectHeight

    print(MeasuredObjectHeight)
    '''

    #CameraRecognition
    FileSizeSet = float(input('Please enter the resolution of the selected camera in MP...'))
    if FileSizeSet == 12.4:
        PicFileSize = 6.5
    else:
        PicFileSize = int(input('Please enter the size of the image captured in MB...'))


Runner()










'''
    elif shape == 6:
  **radius** = float(input("radius: "))
  print( "Perimeter of Circle = ", 2*pi*radius)
'''

'''
#Notes

All calculations are based on 200ft x 200ft parameter

95%
200ft = 9 paths
250ft = 11
300ft = 13
350ft = 15
400ft = 17
450ft = 19
500ft = 20

90
200ft = 5
250ft = 6
300ft = 7
350ft = 8
400ft = 9
450ft = 9
500ft = 11

85
200ft = 4
250ft = 4
300ft = 5
350ft = 6
400ft = 6
450ft = 7
500ft = 8

80
200ft = 3
250ft = 4
300ft = 4
350ft = 5
400ft = 5
450ft = 6
500ft = 6

75
200ft = 3
250ft = 3
300ft = 3
350ft = 4
400ft = 4
450ft = 5
500ft = 5

